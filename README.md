# Vending machine preauth server

This is the pre-auth server for the [Waltham computer-peripherals vending machine](https://corewiki.cimpress.net/wiki/Waltham_vending_machine_for_peripherals).

## Description
This is a Ruby webapp, written with the [Sinatra](http://www.sinatrarb.com/) framework, and intended to run under [Passenger](https://www.phusionpassenger.com/).
It provides the authentication layer for the vending machine. Upon receiving an HTTP GET request at the root with the correct parameters
(something akin to `/?badge_id=\d+`), the app then translates the badge id-number into a {firstname, lastname} tuple, then checks ActiveDirectory for appropriate attributes.
The system that handles physical access (the "_badge system_", powered by [WinDSX](https://corewiki.cimpress.net/wiki/Waltham_WinDSX)) in Waltham
is not integrated with Cimpress' ActiveDirectory infrastructure and only returns basic information about a badge (a first/last name pair). This information isn't unique
to employess, but is good enough to work within any given office.

For more information, see the documentation on the wiki.

## Prerequisites
* [Ruby](https://www.ruby-lang.org/en/), preferably &gt;v2.4, or [RVM](https://rvm.io/)
* [Bundler](http://bundler.io/), library installer &amp; dependency manager
* [git](https://git-scm.com/)

## Installation
* Clone this repository
* Install the libraries using Bundler, a depedency manager for Ruby.
* ``` $ bundler install ```
* Run the app using Passenger
* ```
$ rvm use ruby-2.3.0                     		# Choose your Ruby from RVM
$ rvmsudo bundle exec passenger start
```

## Documentation
* [Waltham vending machine for peripherals](https://corewiki.cimpress.net/wiki/Waltham_vending_machine_for_peripherals)
* [Peripherals vending machine administration](https://corewiki.cimpress.net/wiki/Peripherals_vending_machine_administration)

## See also
* JiraTrain ticket [KM-1520](https://jiratrain101.vistaprint.net/jira/browse/KM-1520)
