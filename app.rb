# ------------------------------------------------------------------------------------------
# Preauth server for the Waltham vending machine
#
# For documentation, see:
#   https://corewiki.cimpress.net/wiki/Peripherals_vending_machine_administration
#
# -------------------------------------------------------------------------------------------
require 'sinatra/base'
require 'net/ldap'
require 'tiny_tds'
require 'set'
require 'yaml'
require 'pp'




# This value must match an "allocation code" setup in the admin portal for the
# vending machine.
#
# IMPORTANT:
#   Whatever you do, don't delete any allocation codes in the admin portal,
#   you cannot create a new one with the same name.
#
AllocationCodeName = 'office'

# An arbitrary value for the allocation code. This exists to provide a bucket "dept"
# in order to setup a global restriction ( like 1 item per day ).
DefaultDepartment = 'WLT'

# Other stuff
$debug = false
Config = YAML.load_file( File.realpath './config.yaml', __dir__ )







# Connects to a database containing information about the physical "security" badges
# issued to humans at Cimpress. The database is very simple and mostly only maps
# badge-ids to names.
class BadgeDatabase
	def initialize
		@client = TinyTds::Client.new(
			:username      => Config['badge_server']['username'],
			:password      => Config['badge_server']['password'],
			:dataserver    => Config['badge_server']['dataserver'],
			:database      => Config['badge_server']['database'],
			:login_timeout => 1,
			:timeout       => 1	)
		@debugging_info = ""
	end

	def get_name( badge_id )
		results = @client.execute( <<-SQL )
			SELECT
			  CARDS.Code AS 'badge_id',
			  NAMES.LName AS 'last_name',
			  NAMES.FName AS 'first_name'
			FROM CARDS
			  INNER JOIN NAMES ON ( CARDS.NameID = NAMES.ID  )
			WHERE
			  Code = #{badge_id}
		SQL
		name = results.first
		if $debug
			@debugging_info = name.pretty_inspect
		end
		return name
	end

	def get_debugging_info
		@debugging_info
	end
end





# Connects to ActiveDirectory (via LDAP) and queries for group membership
# based on first & last name. There is a small chance of name collisions, but for
# now we'll ignore that.
class EmployeeFinder
	def initialize( first_name, last_name )

		# The WinDSX badge-system software transposes apostrophes with backticks
		@first_name = first_name.gsub( "`", "'" )
		@last_name = last_name.gsub( "`", "'" )

		@queried = false
		@client = Net::LDAP.new(
			:host => Config['ldap']['hostname'],
			#:encryption => :simple_tls,
			:auth => {
				:method => :simple,
				:username => Config['ldap']['bind']['dn'],
				:password => Config['ldap']['bind']['password']
			}
		)
		@debugging_info = ""
	end

	def get_personnel_number
		if !@queried
			do_query
		end
		return @entry['extensionattribute2'].to_s.gsub( /\D/, "" )
	end

	def get_physical_delivery_office_name
		if !@queried
			do_query
		end
		return @entry['physicalDeliveryOfficeName']
	end

	def get_debugging_info
		@debugging_info
	end

	private

	def do_query
		filter = Net::LDAP::Filter.construct "(&(objectClass=user)(sn=#{@last_name})(givenName=#{@first_name}))"
		@results = @client.search(
			:filter => filter,
			:base => Config['ldap']['base_dn'],
			:attributes => [ 'sAMAccountName', 'extensionattribute2', 'physicalDeliveryOfficeName' ]
		)

		# Hopefully we only ever find a single person; take the first, ignore the rest.
		# If we don't find anybody, this will fail and throw a 500, which is OK for now.
		@entry = @results.first
		if $debug
			@debugging_info = @entry.pretty_inspect
		end
		@queried = true
	end

end







# The actual webapp, which has a single entry-point at "/".
class PreAuthServerApp < Sinatra::Base

	# The vending machine has certain expectations about HTTP status codes, which somewhat
	# differ from normal interpretation. 200 means the user is authorized to use the machine,
	# 400 or 401 means they are not authorized. Any other status codes (e.g. 403, 405, 5xx)
	# indicates an error of some type.
	Authorized       = 200
	Unauthorized     = 401
	Forbidden        = 403
	NotFound         = 404

	configure :production, :development do
		enable :logging
	end

	helpers do
		def badge_is_allowed( badge_id )
			# Lookup a persons {first,last} name from the security-badge server/database
			@badges = BadgeDatabase.new
			badge_info = @badges.get_name( badge_id )

			# Lookup the employee's record in AD and get their groups
			@ldap = EmployeeFinder.new( badge_info['first_name'], badge_info['last_name'] )

			# Check if the employee is "a Waltham employee" using some AD attributes as an indicator
			is_in_waltham @ldap.get_physical_delivery_office_name
		end

		def is_in_waltham( officename ) 
			Config['required_physicalDeliveryOfficeName'].eql?( officename )
		end

		def generate_response_body
			body = ""
			if $debug
				body += sprintf( "*** params ***\n%s\n\n", params.pretty_inspect )
				body += sprintf( "*** badge system ***\n%s\n\n", @badges.get_debugging_info )
				body += sprintf( "*** active directory ***\n%s\n\n", @ldap.get_debugging_info )
				body += sprintf( "*** is_in_waltham? ***\n%s\n\n", is_in_waltham( @ldap.get_physical_delivery_office_name )   )
				body += sprintf( "*** HTTP status ***\n%s\n\n", @status )
			end
			body += sprintf( "%s|%s|%s\r\n",
							@ldap.get_personnel_number.to_s,
							AllocationCodeName,
							DefaultDepartment )
			return body
		end

	end

	get '/' do
		content_type 'text/plain'
		cache_control 'private'

		$debug = params.key?( 'debug' )

		unless params.key?( 'badge_id' )
			halt Forbidden, "No 'badge_id' parameter given, cannot process request."
		end

		# The vending machine's physical card-reader adds a character at the front of the
		# badge id (typically an extra zero), pull that off.
		id = params['badge_id']
		id = id[1..id.length]

		@status = ( badge_is_allowed( params['badge_id'] ) ) ? Authorized : Unauthorized

		status @status
		body generate_response_body
	end

end
